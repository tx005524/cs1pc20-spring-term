#ifndef INVENTORY_H
#define INVENTORY_H

#include <vector>
#include "GameObject.h" // Make sure this is the correct path to your GameObject class

class Inventory {
private:
    std::vector<GameObject*> items;

public:
    Inventory(); // Constructor declaration
    ~Inventory(); // Destructor declaration

    void addItem(GameObject* item);
    void useItem(GameObject* item, GameObject& target);

    // Copy and move constructors/assignments are deleted to prevent memory leaks or dangling pointers
    Inventory(const Inventory&) = delete;
    Inventory& operator=(const Inventory&) = delete;
    Inventory(Inventory&&) = delete;
    Inventory& operator=(Inventory&&) = delete;
};

#endif // INVENTORY_H
