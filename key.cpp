#include "Key.h"
#include <iostream>

void Key::useOn(GameObject& target) {
    // Example implementation for how a key interacts with a target
    std::cout << "Using the key on target." << std::endl;
}
