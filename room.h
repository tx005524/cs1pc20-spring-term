#ifndef ROOM_H
#define ROOM_H

#include <string>
#include <vector>
#include <map>
#include "item.h"  // Assuming you have an Item class defined elsewhere

class Room {
private:
    std::string description;
    std::map<std::string, Room*> exits;
    std::vector<Item> items;

public:
    Room(const std::string& desc);
    void AddExit(const std::string& direction, Room* room);
    void AddItem(const Item& item);
    std::string GetDescription() const;
    const std::vector<Item>& GetItems() const;
    const std::map<std::string, Room*>& GetExits() const;
    Room* GetExit(const std::string& direction);
};

#endif // ROOM_H
