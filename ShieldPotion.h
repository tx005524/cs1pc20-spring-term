#ifndef SHIELDPOTION_H
#define SHIELDPOTION_H

#include "GameObject.h" // Ensure this includes the base class GameObject

class ShieldPotion : public GameObject {
public:
    explicit ShieldPotion(); // Constructor declaration
    virtual ~ShieldPotion() override; // Virtual destructor

    virtual void useOn(GameObject& target) override; // Override the useOn method
};

#endif // SHIELDPOTION_H
