#ifndef AREA_H
#define AREA_H

#include "room.h"
#include <string>
#include <unordered_map>

class Area {
private:
    // Using an unordered_map for fast lookup of rooms by name
    std::unordered_map<std::string, Room*> rooms;
    Room* startingRoom = nullptr; // Add a pointer to hold the starting room

public:
    Area();  // Constructor
    ~Area(); // Destructor to deallocate any dynamic memory

    // Prevent copying and assignment
    Area(const Area&) = delete;
    Area& operator=(const Area&) = delete;

    void AddRoom(const std::string& name, Room* room); // Add a room
    Room* GetRoom(const std::string& name) const;      // Get a room by name
    void SetStartingRoom(Room* room);                  // Set the starting room
    Room* GetStartingRoom() const;                     // Get the starting room
    void LoadMapFromFile(const std::string& filename); // Load rooms from a file
};

#endif // AREA_H
