#include "room.h"

Room::Room(const std::string& desc) : description(desc) {}

void Room::AddExit(const std::string& direction, Room* room) {
    exits[direction] = room;
}

void Room::AddItem(const Item& item) {
    items.push_back(item);
}

std::string Room::GetDescription() const {
    return description;
}

const std::vector<Item>& Room::GetItems() const {
    return items;
}

const std::map<std::string, Room*>& Room::GetExits() const {
    return exits;
}

Room* Room::GetExit(const std::string& direction) {
    auto it = exits.find(direction);
    return it != exits.end() ? it->second : nullptr;
}
