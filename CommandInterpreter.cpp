#include "CommandInterpreter.h"

// Constructor implementation
CommandInterpreter::CommandInterpreter(Player* player) : player_(player) {}

// Method implementation for interpreting and executing commands
void CommandInterpreter::interpretCommand(const std::string& command) {
    std::istringstream iss(command);
    std::string action;
    iss >> action; // Extract the action/command

    // Using if-else statements to identify and execute the command
    if (action == "move") {
        std::string direction;
        if (iss >> direction) {
            // Process move command
            player_->move(direction);
        } else {
            std::cout << "Move where?" << std::endl;
        }
    } else if (action == "pick") {
        std::string temp, item;
        iss >> temp >> item; // Assume "pick" is followed by "up <item>"
        if (temp == "up" && !item.empty()) {
            // Process pick up item command
            player_->pickUpItem(item);
        } else {
            std::cout << "Couldn't understand that. Did you mean 'pick up <item>'?" << std::endl;
        }
    } else if (action == "look") {
        // Process look command
        player_->lookAround();
    } else {
        std::cout << "Unknown command: " << command << std::endl;
    }
}
