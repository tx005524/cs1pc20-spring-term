#include "character.h"
#include <algorithm> // Include for std::find

Character::Character(const std::string& name, int health)
    : name(name), health(health) {}

void Character::TakeDamage(int damage) {
    health -= damage;
    if (health < 0) {
        health = 0;
    };
      
}

std::string Character::GetName() const {
    return name;
}

int Character::GetHealth() const {
    return health;
}

void Character::SetHealth(int health) {
    this->health = health;
}

void Character::AddItem(const Item& item) {
    inventory.push_back(item);
}

void Character::RemoveItem(const Item& item) {
    auto endIter = std::remove_if(inventory.begin(), inventory.end(),
                                  [&item](const Item& inventoryItem) { return item == inventoryItem; });
    inventory.erase(endIter, inventory.end());
}
