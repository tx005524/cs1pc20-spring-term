#include "item.h"

// Constructor implementation
Item::Item(const std::string& n, const std::string& desc)
    : name(n), description(desc) {}

// GetName method
std::string Item::GetName() const {
    return name;
}

// GetDescription method
std::string Item::GetDescription() const {
    return description;
}

// Interact method
// This needs to be implemented based on your game design.
// Here's a simple example implementation.
void Item::Interact() const {
    std::cout << "You see " << name << ": " << description << std::endl;
}

// Equality operator
bool Item::operator==(const Item& other) const {
    return name == other.name && description == other.description;
}
