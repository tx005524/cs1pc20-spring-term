#include "GameObject.h"

// Even though we specified "= default;" in the header, explicitly defining it here ensures linker happiness
GameObject::~GameObject() = default;
