#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include "character.h" // Include the Character class header
#include "room.h" // Include the Room class header
#include <iostream> // Include for I/O operations

class Player : public Character {
private:
    Room* location;
public:
    Player(const std::string& n, int h);

    void SetLocation(Room* loc);
    Room* GetLocation() const;

    void ShowHealth() const;
    void RecoverHealth(int amount);

    // Added method declarations as per instructions
    void move(const std::string& direction);
    void pickUpItem(const std::string& item);
    void lookAround();
};

#endif // PLAYER_H
