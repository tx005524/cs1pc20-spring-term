#ifndef ITEM_H // Check if ITEM_H is not defined
#define ITEM_H // Define ITEM_H

#include <iostream>
#include <vector>
#include <map>
#include <string>

class Item {
private:
    std::string name;
    std::string description;

public:
    Item(const std::string& n, const std::string& desc);

    std::string GetName() const;
    std::string GetDescription() const;
    void Interact() const; // Declaration only
    bool operator==(const Item& other) const;
};

#endif // ITEM_H
