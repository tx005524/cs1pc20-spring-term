#ifndef COMMANDINTERPRETER_H
#define COMMANDINTERPRETER_H

#include <iostream>
#include <string>
#include <sstream>
#include "player.h"  // Include the Player class header

// CommandInterpreter class declaration
class CommandInterpreter {
public:
    CommandInterpreter(Player* player); // Constructor declaration
    void interpretCommand(const std::string& command); // Method for parsing and executing commands

private:
    Player* player_; // Pointer to a Player object
};

#endif // COMMANDINTERPRETER_H
