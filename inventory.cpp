#include "Inventory.h"
#include <iostream>

Inventory::Inventory() = default;

Inventory::~Inventory() {
    // Cleanup dynamic memory
    for (auto* item : items) {
        delete item;
    }
    items.clear();
}

void Inventory::addItem(GameObject* item) {
    items.push_back(item);
}

void Inventory::useItem(GameObject* item, GameObject& target) {
    if (item) {
        item->useOn(target); // Invoke the useOn method of the item
    }
    else {
        std::cout << "Item pointer is null, cannot use." << std::endl;
    }
}
