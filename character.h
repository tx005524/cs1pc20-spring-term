#ifndef CHARACTER_H
#define CHARACTER_H

#include <string>
#include <vector>
#include "item.h" // Include Item header file

class Character {
private:
    std::string name;
    int health;
    std::vector<Item> inventory;

public:
    Character(const std::string& name, int health);

    void TakeDamage(int damage);
    void SetHealth(int health); // Added method to set health value.

    std::string GetName() const;
    int GetHealth() const;
    void AddItem(const Item& item);
    void RemoveItem(const Item& item);
};

#endif // CHARACTER_H
