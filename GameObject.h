#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

class GameObject {
public:
    virtual ~GameObject() = default; // Ensure proper deletion of derived class objects
    virtual void useOn(GameObject& target) = 0; // Pure virtual function for using the object
};

#endif // GAMEOBJECT_H
