#include "ShieldPotion.h"
#include "Character.h" // This inclusion allows us to treat the target as a Character
#include <iostream>

ShieldPotion::ShieldPotion() = default; // If there's specific initialization, it would go here

ShieldPotion::~ShieldPotion() = default; // Proper destructor

void ShieldPotion::useOn(GameObject& target) {
    // Assuming we can treat our GameObject as a Character
    // This requires dynamic_cast to safely convert GameObject* to Character*
    Character* character = dynamic_cast<Character*>(&target);
    if (character) {
        const int healthBoost = 20; // Just as an example value
        character->SetHealth(character->GetHealth() + healthBoost);
        std::cout << character->GetName() << " used Shield Potion. Health increased by " << healthBoost << "!" << std::endl;
    } else {
        std::cout << "Shield Potion can't be used on this object." << std::endl;
    }
}
