#include "area.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
// Include other necessary headers you might be using

Area::Area() {}

Area::~Area() {
    // Free dynamically allocated Room objects
    for (auto& pair : rooms) {
        delete pair.second; // deallocate Room objects
    }
    rooms.clear();
}

void Area::AddRoom(const std::string& name, Room* room) {
    rooms[name] = room;
}

Room* Area::GetRoom(const std::string& name) const {
    auto found = rooms.find(name);
    if (found != rooms.end()) {
        return found->second;
    }
    return nullptr; // Return nullptr if room not found
}

void Area::SetStartingRoom(Room* room) {
    startingRoom = room;
}

Room* Area::GetStartingRoom() const {
    return startingRoom;
}

void Area::LoadMapFromFile(const std::string& filename) {
    std::ifstream file(filename);
    
    if (!file) {
        std::cerr << "Failed to open " << filename << " for reading." << std::endl;
        return;
    }

    std::string line;
    while (std::getline(file, line)) {
        std::istringstream iss(line);
        std::string roomName, connectedRoomName, direction;
        
        if (std::getline(iss, roomName, '|') &&
            std::getline(iss, connectedRoomName, '|') &&
            std::getline(iss, direction)) {
            
            // Retrieve or create the main room
            Room* room = this->GetRoom(roomName);
            if (!room) {
                room = new Room(roomName);
                this->AddRoom(roomName, room);
            }

            // Retrieve or create the connected room
            Room* connectedRoom = this->GetRoom(connectedRoomName);
            if (!connectedRoom) {
                connectedRoom = new Room(connectedRoomName);
                this->AddRoom(connectedRoomName, connectedRoom);
            }

            // For simplicity, let's assume ConnectTo is a function in Room class accepting the connectedRoom
            // You might have to implement something to keep track of the directions as well
            // This part needs to be adjusted or implemented according to your actual Room class implementation.
         room->AddExit(direction, connectedRoom);
        }
    }
    
    file.close();
    std::cout << "Map loaded successfully from: " << filename << std::endl;
}
