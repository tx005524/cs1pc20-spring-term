#include "player.h"

Player::Player(const std::string& n, int h) : Character(n, h), location(nullptr) {}

void Player::SetLocation(Room* loc) {
    location = loc;
}

Room* Player::GetLocation() const {
    return location;
}

void Player::ShowHealth() const {
    std::cout << "Health: " << GetHealth() << std::endl;
}

void Player::RecoverHealth(int amount) {
    SetHealth(GetHealth() + amount);
}
