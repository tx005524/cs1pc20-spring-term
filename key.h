#ifndef KEY_H
#define KEY_H

#include "GameObject.h"

class Key : public GameObject {
public:
    void useOn(GameObject& target) override;
};

#endif // KEY_H
