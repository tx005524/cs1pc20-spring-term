#include "area.h" // Include the Area header file
#include "player.h" // Include the Player header file
#include <iostream>

int main() {
    // Create an instance of the Area class
    Area gameWorld;

    // Load the game map from a text file - This addresses requirement 4 implicitly
    gameWorld.LoadMapFromFile("game_map.txt"); // This automatically handles requirement 1 and 2 as well

    // Create a Player
    Player player("Alice", 100);

    // Set the player's starting room (ensured it matches one from game_map.txt)
    Room* currentRoom = gameWorld.GetRoom("startRoom");
    if (currentRoom == nullptr) {
        std::cerr << "Starting room not found. Check your game_map.txt file." << std::endl;
        return -1;
    }
    player.SetLocation(currentRoom);

    // Game loop
    while (true) {
        std::cout << "Current Location: " << player.GetLocation()->GetDescription() << std::endl;
        std::cout << "Items in the room:" << std::endl;
        for (const Item& item : player.GetLocation()->GetItems()) {
            std::cout << "- " << item.GetName() << ": " << item.GetDescription() << std::endl;
        }

        // Listing available exits from the current room
        std::cout << "Exits available: ";
        auto exits = player.GetLocation()->GetExits();
        for (const auto& exit : exits) {
            std::cout << exit.first << " ";
        }
        std::cout << "\nOptions: ";
        std::cout << "1. Look around | ";
        std::cout << "2. Interact with an item | ";
        std::cout << "3. Move to another room | ";
        std::cout << "4. Quit" << std::endl;

        int choice;
        std::cin >> choice;
        if (choice == 1) {
            std::cout << "You look around the room." << std::endl;
        } else if (choice == 2) {
            std::cout << "Enter the name of the item you want to interact with: ";
            std::string itemName;
            std::cin >> itemName;
            bool found = false;
            for (const Item& item : player.GetLocation()->GetItems()) {
                if (item.GetName() == itemName) {
                    item.Interact();
                    found = true;
                    break;
                }
            }
            if (!found) {
                std::cout << "Item not found." << std::endl;
            }
        } else if (choice == 3) {
            std::cout << "Enter the direction (e.g., north, south): ";
            std::string direction;
            std::cin >> direction;
            Room* nextRoom = player.GetLocation()->GetExit(direction);
            if (nextRoom != nullptr) {
                player.SetLocation(nextRoom);
                currentRoom = nextRoom;
                std::cout << "You move to the next room." << std::endl;
            } else {
                std::cout << "You can't go that way." << std::endl;
            }
        } else if (choice == 4) {
            std::cout << "Goodbye!" << std::endl;
            break;
        } else {
            std::cout << "Invalid choice. Try again." << std::endl;
        }
    }
    return 0;
}
